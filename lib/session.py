import base64
import datetime
import hashlib
import hmac
import json
import requests
import sys
import time

import lib.request as request


class Session(object):
    def __init__(self, session_type, keys):
        if session_type == 'live':
            self._base_url = 'https://api.gemini.com'
        elif session_type == 'sandbox':
            self._base_url = 'https://api.sandbox.gemini.com'
        else:
            print(f'Invalid session type: "{session_type}".')
            sys.exit(1)
        keys = keys[session_type]
        self._api_key = keys['apikey']
        self._secret_key = keys['secret'].encode()

    def execute(self, req):
        if type(req) is request.Public:
            return self.execute_public_request(req.url_suffix)
        elif type(req) is request.Private:
            return self.execute_private_request(req.payload)

    def execute_public_request(self, url_suffix):
        return requests.get(self._base_url + url_suffix).json()

    def execute_private_request(self, payload):
        t = datetime.datetime.now()
        payload['nonce'] = str(int(time.mktime(t.timetuple()) * 1000))

        encoded_payload = json.dumps(payload).encode()
        b64 = base64.b64encode(encoded_payload)
        signature = hmac.new(self._secret_key, b64, hashlib.sha384).hexdigest()

        request_headers = {'Content-Type': 'text/plain',
                           'Content-Length': '0',
                           'X-GEMINI-APIKEY': self._api_key,
                           'X-GEMINI-PAYLOAD': b64,
                           'X-GEMINI-SIGNATURE': signature,
                           'Cache-Control': 'no-cache'}

        response = requests.post(self._base_url + payload['request'], data=None,
                                 headers=request_headers)
        return response.json()
