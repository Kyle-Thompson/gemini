#!/usr/bin/python

import argparse
import json
import toml

from actions.order import Order
from actions.listorders import ListOrders
from actions.symbol import Symbol
from lib.session import Session


def parse_args():
    parser = argparse.ArgumentParser()

    # global settings
    parser.add_argument('--session', help='Live or sandbox.', default='sandbox')
    parser.add_argument('--config', help='Path to config file.',
                        default='config.toml')

    # action options
    subparsers = parser.add_subparsers(title='action',
                                       help='The action to take.',
                                       dest='action')
    # TODO: loop through all available actions
    Order.cli_args(subparsers)
    ListOrders.cli_args(subparsers)
    Symbol.cli_args(subparsers)

    return parser.parse_args()


def make_request(args):
    if args.action == 'buy' or args.action == 'sell':
        return (Order.Builder()
                .symbol(args.symbol)
                .amount(args.amount)
                .price(args.price)
                .side(args.action)
                .trade_type(args.type)
                .with_options(args.options.split(','))
                .build())
    elif args.action == 'list-orders':
        return ListOrders()
    elif args.action == 'symbol':
        return (Symbol.Builder()
                .symbol(args.symbol)
                .build())
    else:
        print(f'Unrecognized command "{args.action}".')
        quit(1)


args = parse_args()
request = make_request(args).request()
session = Session(args.session, toml.load(args.config))
response = session.execute(request)
print(json.dumps(response, indent=2))
