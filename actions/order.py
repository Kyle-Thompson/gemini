""" Place an order.

Usage example:
    gemini.py buy --symbol ethusd --amount 0.1 --price 1998.00 \
            --type 'exchange limit' --options maker-or-cancel
"""

import lib.request as request


class Order(object):
    class Builder(object):
        def __init__(self):
            self._symbol = None
            self._amount = None
            self._price = None
            self._side = None
            self._trade_type = None
            self._options = []

        def symbol(self, symbol):
            self._symbol = symbol
            return self

        def amount(self, amount):
            self._amount = amount
            return self

        def price(self, price):
            self._price = price
            return self

        def side(self, side):
            self._side = side
            return self

        def trade_type(self, trade_type):
            self._trade_type = trade_type
            return self

        def with_option(self, option):
            self._options.append(option)
            return self

        def with_options(self, options):
            self._options.extend(options)
            return self

        def _validate(self):
            pass

        def build(self):
            self._validate()
            return Order(symbol=self._symbol, amount=self._amount,
                         price=self._price, side=self._side,
                         trade_type=self._trade_type, options=self._options)

    def __init__(self, symbol, amount, price, side, trade_type, options):
        self._symbol = symbol
        self._amount = amount
        self._price = price
        self._side = side
        self._trade_type = trade_type
        self._options = options

    def request(self):
        return request.Private({
            'request': '/v1/order/new',
            'symbol': self._symbol,
            'amount': self._amount,
            'price': self._price,
            'side': self._side,
            'type': self._trade_type,
            'options': self._options
        })

    @staticmethod
    def cli_args(subparser):
        api_url = 'TODO'
        # TODO: currently this only supports limit orders. expand.
        parser_buy = subparser.add_parser('buy', help='Send a buy order.')
        parser_sell = subparser.add_parser('sell', help='Send a sell order.')
        for p in [parser_buy, parser_sell]:
            p.add_argument('-s', '--symbol',
                           help='The symbol to trade. (e.g. ethusd)',
                           required=True)
            p.add_argument('-a', '--amount',
                           help='The amount of the symbol to trade.',
                           required=True)
            p.add_argument('-p', '--price',
                           help='The price to trade at',
                           required=True)
            p.add_argument('-t', '--type',
                           help=('The type of trade to execute. (e.g. exchange '
                                 'limit)'),
                           default='exchange limit')
            p.add_argument('-o', '--options',
                           help=('Any additional options for the trade '
                                 f'specified here {api_url}'),
                           required=True)
