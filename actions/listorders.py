import lib.request as request


class ListOrders(object):
    def __init__(self):
        pass

    def payload(self):
        return request.Private({
            'request': '/v1/orders',
        })

    @staticmethod
    def cli_args(subparser):
        subparser.add_parser('list-orders',
                             help='Print a list of all active orders.')
